provider "aws" {
  region = "us-east-1"
}

resource "aws_security_group" "tcp22__access" {
  name        = "terraformpuppetsecuritygroup"
  description = "terraform puppet integration group"
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow SSH access from anywhere (you might want to restrict this to your IP range)
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    # Allow outbound traffic to port 443 (HTTPS)
}
egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}
}

resource "aws_instance" "puppetagentchinmay" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "terraformpuppetintegrationkey"
  vpc_security_group_ids = [aws_security_group.tcp22__access.id]
  tags = {
    Name = "puppetagentchinmay"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("E:/gitlab/assignment4/puppetterraformintegration/terraformpuppetintegrationkey.pem")
    host        = self.public_ip
    timeout     = "5m" # we can adjust it as per our requirenments
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.28.194 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"
    ]
  }
}

output "public_ip" {
  value = aws_instance.puppetagentchinmay.public_ip
}
