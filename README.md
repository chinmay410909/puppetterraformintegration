# Configuration Management with Puppet and Terraform By Chinmay

## Introduction

Integrating Terraform with Puppet allows organizations to combine the infrastructure provisioning capabilities of Terraform with the configuration management capabilities of Puppet, enabling end-to-end automation of infrastructure deployment and configuration.

## Prerequisites

Before starting with Terraform, we must have the follwing things:

- **Terraform**: 
Install Terraform on your local machine or on the system where you plan to manage your infrastructure. You can download Terraform from the [official website](https://www.terraform.io).

- **Account on Cloud Service Providers:**
You must have your account on Cloud Service Providers like AWS, Azure or Google Cloud Platform.

- **Puppet Server or Agent:** Set up a Puppet server to manage your configurations or Puppet agents on the target nodes where you want to apply configurations. We will set up agent only.

## Installation Steps:

- Clone this gitlab repository

`git clone https://gitlab.com/chinmay410909/puppetterraformintegration.git`

- Navigate to the terraform directory

## Configuration Details

**1. Terraform Configuration:**
Create a Terraform configuration to provision infrastructure components. Here's an example Terraform configuration to provision an AWS EC2 instance
```
provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "puppetagentchinmay" {
  ami           = "ami-07d9b9ddc6cd8dd30"
  instance_type = "t2.micro"
  key_name      = "terraformpuppetintegrationkey"
  vpc_security_group_ids = [aws_security_group.tcp22__access.id]
  tags = {
    Name = "puppetagentchinmay"
  }
```

**2. Puppet Manifests:**
Write Puppet manifests to configure the provisioned infrastructure components. Here's an example Puppet manifest to install and configure a package on the provisioned instance

``` 
package { 'apache2':
ensure => present, }
service { 'apache2': ensure => running, enable => true,
}
```

**3. Integration**
To integrate Terraform with Puppet, we have a few options:

- Use Terraform provisioners (remote-exec, local-exec, etc.) to trigger Puppet runs on the provisioned instances.
- Utilize configuration management tools like Ansible or Shell scripts within Terraform to execute Puppet runs remotely on the provisioned instances.

Let's go with first options
Here is the example :

```
 connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("E:/gitlab/assignment4/puppetterraformintegration/terraformpuppetintegrationkey.pem")
    host        = self.public_ip
    timeout     = "5m" # we can adjust it as per our requirenments
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.28.194 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"
    ]
  }

```

## Running The Integration 

- Open Command prompt or terminal and go to terraform folder

`cd Terraform`

- Edit the credentials as per your AWS account like ami id, keypair, etc.

- Run the following commands

`terraform init`: Initializes a Terraform working directory by downloading providers and modules.

`terraform validate`: Checks the configuration for syntax errors and other issues.

`terraform plan`: Creates an execution plan, showing changes to be applied to infrastructure.

`terraform apply`: Applies changes to the infrastructure as described in the execution plan.

- After completing work 

`terraform destroy`: Destroys the Terraform-managed infrastructure.

## Testing 

You can see whether it is reflected on cloud or not...
See screenshots how it will look

![](screenshots/ec2ubuntu.png)
- Puppet Suceefully running :

![](screenshots/agentrun.png)
